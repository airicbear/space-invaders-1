package gui;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import object.GameObject;
import spaceinvaders.Game;
import spaceinvaders.asset.Invader;
import spaceinvaders.asset.SpaceShip;

public class Graphics {

  public static void drawEnvironment(Game environment, GraphicsContext ctx) {
    drawBackground(environment, ctx);
    drawScore(environment.spaceShip, ctx);
    drawLifePoints(environment.spaceShip, ctx);
    drawSpaceShip(environment.spaceShip, ctx);
    if(!environment.spaceShip.getProjectile().isIdle()) {
      drawGameObject(environment.spaceShip.getProjectile(), ctx);
    }
    for(Invader[] invader : environment.getInvaders()) {
      for(Invader invader2 : invader) {
        drawInvader(invader2, ctx);
        drawGameObject(invader2.getProjectile(), ctx);
      }
    }
  }
  
  public static void drawBackground(Game environment, GraphicsContext ctx) {
    ctx.setFill(Game.color);
    ctx.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);
  }
  
  public static void drawGameObject(GameObject gameObject, GraphicsContext ctx) {
    ctx.setFill(gameObject.color);
    ctx.fillRect(gameObject.position.x, gameObject.position.y, gameObject.scale.x, gameObject.scale.y);
  }
  
  public static void drawSpaceShip(SpaceShip spaceShip, GraphicsContext ctx) {
    ctx.setFill(spaceShip.color);
    
    // Body
    float bodyScaleX = spaceShip.scale.x;
    float bodyScaleY = (spaceShip.scale.y / 2) - 10;
    float bodyPosX = spaceShip.position.x;
    float bodyPosY = spaceShip.position.y;
    ctx.fillRect(bodyPosX+5, bodyPosY-5, bodyScaleX-10, bodyScaleY-5);
    ctx.fillRect(bodyPosX, bodyPosY, bodyScaleX, bodyScaleY);
    
    // Cannon
    float cannonWidth = 2.5f;
    float cannonLength = 15;
    float cannonPosX = spaceShip.position.x + (spaceShip.scale.x / 2) - (cannonWidth / 2);
    float cannonPosY = spaceShip.position.y - cannonLength;
    ctx.fillRect(cannonPosX - 3, cannonPosY + 3, (cannonWidth + 6), cannonLength);
    ctx.fillRect(cannonPosX, cannonPosY, cannonWidth, cannonLength);
  }
  
  public static void drawInvader(Invader invader, GraphicsContext ctx) {
    ctx.setFill(invader.color);
    ctx.fillRect(invader.position.x, invader.position.y, invader.scale.x, invader.scale.y);
  }
  
  public static void drawScore(SpaceShip spaceShip, GraphicsContext ctx) {
    ctx.setFill(spaceShip.color);
    ctx.fillText("Score: " + spaceShip.getScore(), 10, Game.HEIGHT - 10);
  }
  
  public static void drawLifePoints(SpaceShip spaceShip, GraphicsContext ctx) {
    ctx.setFill(spaceShip.color);
    ctx.fillText("Lives: " + spaceShip.getLifePoints(), Game.WIDTH - 57.5f, Game.HEIGHT - 10);
  }
  
  public static void drawPaused(GraphicsContext ctx) {
    ctx.setFill(Color.AQUAMARINE);
    ctx.fillText("PAUSED", Game.WIDTH / 2 - 25, Game.HEIGHT / 2);
  }
  
  public static void drawRetry(GraphicsContext ctx) {
    ctx.setFill(Color.AQUAMARINE);
    ctx.fillText("HIT ENTER TO RETRY", Game.WIDTH / 2 - 55, Game.HEIGHT / 2);
  }
}
