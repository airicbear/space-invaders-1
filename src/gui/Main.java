package gui;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import object.Movable;
import spaceinvaders.Game;
import spaceinvaders.asset.Invader;
import spaceinvaders.asset.SpaceShip;
import vector.Vector2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.animation.AnimationTimer;

public class Main extends Application {
    
  private Game environment;
  
  private GraphicsContext ctx;
  private AnimationTimer mainLoop;
  
  private static boolean loopRunning = true;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    
    // Initialize variables
    StackPane root = new StackPane();
    Canvas canvas = new Canvas(spaceinvaders.Game.WIDTH, spaceinvaders.Game.HEIGHT);
    ctx = canvas.getGraphicsContext2D();
    environment = new Game(); 
    
    // Configure variables
    canvasConfig(canvas);
    stageConfig(stage);
    
    // Setup
    root.getChildren().add(canvas);
    stage.setScene(new Scene(root));
    stage.show();
    
    // Loop
    mainLoop = new AnimationTimer() {
      
      @Override
      public void handle(long now) {
        if(loopRunning) {
          
          // Spaceship death
          if(environment.spaceShip.isDead()) {
            environment.spaceShip.color = Color.RED;
            environment.spaceShip.freeze();
          }
        
          // Update spaceship position
          if(environment.inBounds(environment.spaceShip) && !environment.spaceShip.isIdle()) {
            environment.spaceShip.move();
          }
        
          // Update projectile position
          if(!environment.spaceShip.getProjectile().isIdle()) {
            (new Thread(new ProjectileThread())).start();
          }
        
          // Update invaders' projectiles' positions
          (new Thread(new InvaderProjectileThread())).start();
        
          // Update invaders' positions
          (new Thread(new InvaderMovementThread())).start();
        
        
          // Invaders shoot
          (new Thread(new InvaderShootThread())).start();
        
          // Respawn invaders when all are dead
          if(environment.invaders.isEmpty()) {
            environment.invaders.respawn();
          }
        
          // Draw game
          Graphics.drawEnvironment(environment, ctx);
        
          // Draw retry upon spaceship death
          if(environment.spaceShip.isDead()) {
            Graphics.drawRetry(ctx);
          }
        }
      }
      
    };
    mainLoop.start();
  }
  
  public class InvaderShootThread implements Runnable {
    
    private long lastTime = System.nanoTime();

    @Override
    public void run() {
      long time = System.nanoTime();
      long invaderShootInterval = 2700000;
      if(time - lastTime > invaderShootInterval) {
        environment.invaders.getRandomBottomInvader().shoot(Vector2D.DOWN);
        lastTime = time;
      }
    }
    
  }
  
  public void restart() {
    environment = new Game();
  }
  
  public class InvaderMovementThread implements Runnable{

    @Override
    public void run() {
      Invader[] row = environment.getInvaders()[0];
      if(environment.inBounds(row[row.length-1]) && environment.inBounds(row[0])) {
        environment.invaders.moveInvaders();
      } else {
        environment.invaders.changeInvadersDirection();
      }
    }
    
  }
  
  public class InvaderProjectileThread implements Runnable {
    
    @Override
    public void run() {
      environment.invaders.moveInvaderProjectiles(environment);
      if(environment.spaceShipHit()) {
        environment.spaceShip.subtractLifePoint();
        environment.spaceShip.color = Color.RED;
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      environment.spaceShip.color = Color.LIGHTGREEN;
    }
    
  }
  
  public class ProjectileThread implements Runnable {

    @Override
    public void run() {
      Movable projectile = environment.spaceShip.getProjectile();
      if(!environment.invaders.invaderHit(projectile) && !environment.inBounds(projectile)) {
        projectile.move();
      } else {
        projectile.freeze();
      }
    }
    
  }
  
  private void stageConfig(Stage stage) {
    stage.setResizable(false);
    stage.setTitle("Space Invaders");
    stage.setOnCloseRequest(e -> System.exit(0));
  }
  
  private void canvasConfig(Canvas canvas) {
    canvas.setFocusTraversable(true);
    canvas.setOnKeyPressed(new KeyPressHandler());
    canvas.setOnKeyReleased(new KeyReleaseHandler());
  }

  private class KeyPressHandler implements EventHandler<KeyEvent> {

    @Override
    public void handle(KeyEvent e) {
      handleCannon(environment.spaceShip, e);
      handleReset(e);
    }
    
    public void handleCannon(SpaceShip spaceShip, KeyEvent e) {
      switch(e.getCode()) {
        case A:
        case LEFT:
          spaceShip.velocity = Vector2D.LEFT.multiply(spaceShip.getMovementSpeed());
          break;
        case D:
        case RIGHT:
          spaceShip.velocity = Vector2D.RIGHT.multiply(spaceShip.getMovementSpeed());
          break;
        case SPACE:
          if(!spaceShip.isDead()) {
            spaceShip.shoot(Vector2D.UP);
          }
          break;
        case L:
          environment.invaders.getRandomBottomInvader().shoot(Vector2D.DOWN);
          break;
        case R:
          environment.invaders.respawn();
          break;
        default:
          break;
      }
    }
    
    public void handleReset(KeyEvent e) {
      switch (e.getCode()) {
        case ENTER:
          restart();
          break;
        case ESCAPE:
          if(loopRunning) {
            Graphics.drawPaused(ctx);
            mainLoop.stop();
            loopRunning = false;
          } else {
            mainLoop.start();
            loopRunning = true;
          }
          break;
        default:
          break;
      }
    }
  }
  
  private class KeyReleaseHandler implements EventHandler<KeyEvent> {

    @Override
    public void handle(KeyEvent e) {
      handleCannon(environment.spaceShip, e);
    }
    
    public void handleCannon(SpaceShip cannon, KeyEvent e) {
      switch(e.getCode()) {
        case A:
        case D:
        case LEFT:
        case RIGHT:
          cannon.velocity = Vector2D.ZERO;
          break;
        default:
          break;
      }
    }
  }
}
