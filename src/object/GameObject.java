package object;

import javafx.scene.paint.Color;
import vector.Vector2D;

public class GameObject {
  public Color color;
  public Vector2D position, scale;
  
  public GameObject() {
    color = Color.WHITE;
    position = Vector2D.ZERO;
    scale = Vector2D.ZERO;
  }
  
  public GameObject(float x, float y, float w, float h) {
    color = Color.WHITE;
    position = new Vector2D(x, y);
    scale = new Vector2D(w, h);
  }
  
  /**
   * @param other
   * @return True if colliding with other game object
   */
  public boolean colliding(GameObject other) {
    boolean checkTop, checkBottom, checkLeft, checkRight;
    checkTop = position.y - (scale.y / 4) <= other.position.y;
    checkBottom = position.y + scale.y >= other.position.y;
    checkLeft = position.x <= other.position.x;
    checkRight = position.x + scale.x >= other.position.x;
    return checkTop && checkBottom && checkLeft && checkRight;
  }
  
  public void highlight(Color color) {
    this.color = color;
  }
}
