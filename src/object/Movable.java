package object;

import vector.Vector2D;

public class Movable extends GameObject {
  public Vector2D velocity;
  protected float speed;
  
  public Movable() {
    velocity = Vector2D.ZERO;
    speed = 10;
  }
  
  public float getMovementSpeed() {
    return speed;
  }
  
  public void move() {
    position = position.add(velocity);
  }

  public boolean isIdle() {
    return velocity == Vector2D.ZERO;
  }
  
  public void freeze() {
    velocity = Vector2D.ZERO;
  }
}
