package scene;

import javafx.scene.paint.Color;
import object.GameObject;
import object.Movable;
import spaceinvaders.Game;
import vector.Vector2D;

public abstract class Scene {
  public static final Vector2D SIZE = new Vector2D(500, 500);
  public static final Vector2D CENTER = SIZE.multiply(0.5f);
  public static final float WIDTH = SIZE.x;
  public static final float HEIGHT = SIZE.y;
  public static Color color;
  
  public Scene() {
    color = Color.BLACK;
  }
  
  public boolean inBounds(GameObject gameObject) {
    return inLeftBound(gameObject) && inRightBound(gameObject);
  }
  
  public boolean inBounds(Movable movable) {
    return (inLeftBound(movable) && movable.velocity.x < 0) || (inRightBound(movable) && movable.velocity.x > 0);
  }
  
  private boolean inLeftBound(GameObject gameObject) {
    return gameObject.position.x > 0;
  }
  
  private boolean inRightBound(GameObject gameObject) {
    return gameObject.position.x < Game.WIDTH - gameObject.scale.x;
  }
}
