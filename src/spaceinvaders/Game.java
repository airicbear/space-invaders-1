package spaceinvaders;

import javafx.scene.paint.Color;
import object.Movable;
import scene.Scene;
import spaceinvaders.asset.Invader;
import spaceinvaders.asset.SpaceShip;
import vector.Vector2D;

public class Game extends Scene {
  
  public final SpaceShip spaceShip;
  public final InvaderGroup invaders;
  
  public Game() {
    color = Color.BLACK;
    spaceShip = new SpaceShip();
    invaders = new InvaderGroup();
    invaders.spaceShip = spaceShip;
  }
  
  public Invader[][] getInvaders() {
    return invaders.getList();
  }
  
  public boolean spaceShipHit() {
    for(Movable[] row : invaders.getInvaderProjectiles()) {
      for(Movable projectile : row) {
        if(spaceShip.colliding(projectile)) {
          projectile.position = Vector2D.ZERO;
          return true;
        }
      }
    }
    return false;
  }
  
}
