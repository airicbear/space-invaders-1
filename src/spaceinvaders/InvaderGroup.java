package spaceinvaders;

import java.util.Random;

import javafx.scene.paint.Color;
import object.Movable;
import spaceinvaders.asset.Invader;
import spaceinvaders.asset.SpaceShip;
import vector.Vector2D;

public class InvaderGroup {
  
  private Invader[][] invaders;
  public SpaceShip spaceShip;
  
  public InvaderGroup() {
    invaders = new Invader[5][11];
    for(int r = 0; r < invaders.length; r++) {
      for(int c = 0; c < invaders[r].length; c++) {
        invaders[r][c] = new Invader();
        float invaderSpacing = 5;
        float invaderPosX = c*invaders[r][c].scale.x+invaderSpacing*c;
        float invaderPosY = r*invaders[r][c].scale.y+invaderSpacing*r;
        invaders[r][c].position = new Vector2D(invaderPosX+invaders[r][c].scale.x, invaderPosY+50);
        invaders[r][c].setValue((invaders.length - r) * 1000);
      }
    }
  }
  
  public void moveInvaders() {
    for(Invader[] invaderRow : invaders) {
      for(Invader invader : invaderRow) {
        invader.move();
      }
    }
  }
  
  public void changeInvadersDirection() {
    for(Invader[] invaderRow : invaders) {
      for(Invader invader : invaderRow) {
        invader.changeDirection();
      }
    }
  }
  
  public boolean invaderHit(Movable projectile) {
    boolean hit = false;
    for(Invader[] row : invaders) {
      for(Invader invader : row) {
        if(invader.colliding(projectile)) {
          hit = true;
          invader.die();
          spaceShip.addScore(invader.getValue());
          break;
        }
      }
    }
    return hit;
  }
  
  public Movable[][] getInvaderProjectiles() {
    Movable[][] invaderProjectiles = new Movable[invaders.length][invaders[0].length];
    for(int r = 0; r < invaders.length; r++) {
      for(int c = 0; c < invaders[r].length; c++) {
        invaderProjectiles[r][c] = invaders[r][c].getProjectile();
      }
    }
    return invaderProjectiles;
  }
  
  public void moveInvaderProjectiles(Game game) {
    for(Movable[] row : getInvaderProjectiles()) {
      for(Movable invaderProjectile : row) {
        if(!game.inBounds(invaderProjectile)) {
          invaderProjectile.move();
        } else {
          invaderProjectile.freeze();
        }
      }
    }
  }
  
  public Invader getRandomBottomInvader() {
    Random random = new Random();
    int nextRow = random.nextInt(invaders.length);
    int nextCol = random.nextInt(invaders[0].length);
    while(!isBottomInvader(invaders[nextRow][nextCol])) {
      nextRow = random.nextInt(invaders.length);
      nextCol = random.nextInt(invaders[0].length);
    }
    return invaders[nextRow][nextCol];
  }
  
  public boolean isBottomInvader(Invader invader) {
    boolean isBottomInvader = false;  
    for(int c = 0; c < invaders[0].length; c++) {
      if(invader.equals(getBottomInvader(c))) {
        isBottomInvader = true;
      }
    }
    return isBottomInvader;
  }
  
  public Invader getBottomInvader(int index) {
    Invader nextInvader;
    int count = 1;
    do {
      nextInvader = col(index)[col(index).length-count]; 
      count++;
    } while(nextInvader.scale == Vector2D.ZERO && count <= invaders.length);
    return nextInvader;
  }
  
  public void highlightBottomInvaders() {
    for(int r = 0; r < invaders.length; r++) {
      for(int c = 0; c < invaders[r].length; c++) {
        if(invaders[r][c].equals(getBottomInvader(c))) {
          invaders[r][c].color = Color.RED;
        }
      }
    }
  }
  
  public Invader[] row(int index) {
    Invader[] row = new Invader[invaders[0].length];
    for(int i = 0; i < row.length; i++) {
      row[i] = invaders[index][i];
    }
    return row;
  }
  
  public void highlightRow(int index) {
    for(Invader invader : row(index)) {
      invader.highlight(Color.RED);
    }
  }
  
  public Invader[] col(int index) {
    Invader[] col = new Invader[invaders.length];
    for(int i = 0; i < col.length; i++) {
      col[i] = invaders[i][index];
    }
    return col;
  }
  
  public void highlightColumn(int index) {
    for(Invader invader : col(index)) {
      invader.highlight(Color.RED);
    }
  }
  
  public Invader[] lastCol() {
    Invader[] col;
    int count = 1;
    do {
      col = col(invaders[0].length - count);
      count++;
    } while(isEmptyCol(invaders[0].length - count) && count < invaders[0].length);
    return col;
  }
  
  public boolean isEmptyCol(int index) {
    for(Invader invader : col(index)) {
      if(!invader.isDead()) {
        return false;
      }
    }
    return true;
  }
  
  public void highlightLastCol() {
    for(Invader invader : lastCol()) {
      invader.color = Color.RED;
    }
  }
  
  public Invader[][] getList() {
    return invaders;
  }
  
  public void respawn() {
    for(Invader[] row : invaders) {
      for(Invader invader : row) {
        invader.respawn();
      }
    }
  }
  
  public boolean isEmpty() {
    for(Invader[] row : invaders) {
      for(Invader invader : row) {
        if(!invader.isDead()) {
          return false;
        }
      }
    }
    return true;
  }
  
}
