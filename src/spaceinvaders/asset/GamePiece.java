package spaceinvaders.asset;

import javafx.scene.paint.Color;
import object.Movable;
import vector.Vector2D;

public abstract class GamePiece extends Movable {
  
  protected Movable projectile;
  
  public GamePiece() {
    position = Vector2D.ZERO;
    scale = Vector2D.ZERO;
    velocity = Vector2D.ZERO;
    color = Color.WHITE;
    speed = 5;
    projectile = new Movable();
  }
  
  /**
   * Fire a projectile from the game piece
   */
  public abstract void shoot(Vector2D direction);
  
  public abstract boolean isDead();
  
  public Movable getProjectile() {
    return projectile;
  }
   
}
