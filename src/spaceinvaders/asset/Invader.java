package spaceinvaders.asset;

import javafx.scene.paint.Color;
import object.Movable;
import spaceinvaders.Game;
import vector.Vector2D;

public class Invader extends GamePiece {
  
  private int value;
  private int size;

  public Invader() {
    size = 35;
    this.scale = new Vector2D(size, size-10);
    this.position = Vector2D.ZERO;
    this.velocity = Vector2D.RIGHT;
    color = Color.WHITE;
    value = 1;
  }
  
  public String toString() {
    return "Invader @ " + position;
  }
  
  public void changeDirection() {
    velocity = velocity.multiply(-1);
  }
  
  public boolean isDead() {
    return scale == Vector2D.ZERO;
  }
  
  public void die() {
    scale = Vector2D.ZERO;
  }
  
  public void shoot(Vector2D direction) {
    if(!isDead() && (projectile.isIdle() || projectile.position.y < 0 || projectile.position.y > Game.HEIGHT)) {
      Movable projectileClone = new Movable();
      projectileClone.color = this.color;
      projectileClone.scale = new Vector2D(2.5f, 2.5f);
      projectileClone.position = new Vector2D(position.x + (scale.x / 2) - (projectileClone.scale.x / 2), position.y+direction.y*25);
      projectileClone.velocity = direction.multiply(5);
      this.projectile = projectileClone;
    }
  }
  
  public void setValue(int value) {
    this.value = value;
  }
  
  public int getValue() {
    return value;
  }
  
  public int getSize() {
    return size;
  }
  
  public void respawn() {
    scale = new Vector2D(size, size-10);
  }

}
