package spaceinvaders.asset;

import javafx.scene.paint.Color;
import object.Movable;
import spaceinvaders.Game;
import vector.Vector2D;

public class SpaceShip extends GamePiece {
  
  private int score;
  private int lifePoints;
  
  public SpaceShip() {
    this.scale = new Vector2D(50, 50);
    this.position = new Vector2D(Game.CENTER.x - (this.scale.x / 2), Game.HEIGHT-scale.y);
    this.color = Color.LIGHTGREEN;
    this.score = 0;
    this.lifePoints = 3;
  }
  
  public void shoot(Vector2D direction) {
    if(projectile.isIdle() || projectile.position.y < 0 || projectile.position.y > Game.HEIGHT) {
      Movable projectileClone = new Movable();
      projectileClone.color = this.color;
      projectileClone.scale = new Vector2D(2.5f, 10);
      projectileClone.position = new Vector2D(position.x + (scale.x / 2) - projectileClone.scale.x, position.y + direction.y * 25);
      projectileClone.velocity = direction.multiply(10);
      this.projectile = projectileClone;
    }
  }
  
  public boolean isDead() {
    return lifePoints <= 0;
  }
  
  public void addScore(int score) {
    this.score += score;
  }
  
  public int getScore() {
    return score;
  }
  
  public void subtractLifePoint() {
    if(lifePoints > 0) {
      lifePoints -= 1;
    }
  }
  
  public void addLifePoint() {
    lifePoints += 1;
  }
  
  public int getLifePoints() {
    return lifePoints;
  }
  
}
