package vector;

public class Vector2D {
  
  /** Vector2D(0, 0) **/ public static final Vector2D ZERO = new Vector2D(0, 0);
  /** Vector2D(0, -1) **/ public static final Vector2D UP = new Vector2D(0, -1);
  /** Vector2D(0, 1) **/ public static final Vector2D DOWN = new Vector2D(0, 1);
  /** Vector2D(-1, 0) **/ public static final Vector2D LEFT = new Vector2D(-1, 0);
  /** Vector2D(1, 0) **/ public static final Vector2D RIGHT = new Vector2D(1, 0);
  public float x, y;
  
  public Vector2D(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  public Vector2D add(Vector2D other) {
    return new Vector2D(x + other.x, y + other.y);
  }
  
  public String toString() {
    return "<" + x + ", " + y + ">";
  }

  public Vector2D multiply(float n) {
    return new Vector2D(x * n, y * n);
  }
}
